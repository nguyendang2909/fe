import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppService } from './service/app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  giatien = {}; //show price
  textNotification = ''; //show and check text notification when Get Quote is clicked
  checktextNotification = false;  
  hidecreateship = true;  // enable or disable "Create Shipment" button
  hidegetshipment = true;
  hidetextgetshipment = true;
  textgetshipment = ''
  ref = '';
  datagetship ={};
  constructor(private appService: AppService){
  }
  

  getQuote(data){
      
    return this.appService.getQuote(data).subscribe((res: any) => {
      console.log(res);
      
      if (res.statusGetQuote == false) {this.hidecreateship = true;
        this.textNotification = 'Input Sender Country, Receiver Country and Package Weight'
        this.checktextNotification = false;
            }
      else {this.hidecreateship = false;
        this.checktextNotification = true;}
      this.giatien = res.giatien;
      
      console.log(res.statusGetQuote);
      
      
  });
  }

  createShipment(shipment){
    return this.appService.createShipment(shipment).subscribe((shipment: any) => {
      this.hidecreateship = true;
      this.hidegetshipment = false;
      this.ref = shipment.ref
      console.log(shipment);
      
                    
  });

  };

  getShip(getshipment){
    return this.appService.getShip(getshipment).subscribe((getshipment: any) => {

      if (getshipment.statusGetShipment == false) {
        this.hidetextgetshipment = true;
        this.textgetshipment = 'Not found'
      }
      else{
      this.datagetship = getshipment.resultGetShip;
      this.hidetextgetshipment = false;
      console.log(getshipment)
      console.log(this.datagetship);}
      
      
  });
  }

  deleteShip(deleteshipment){
    return this.appService.deleteShip(deleteshipment).subscribe((deleteshipment: any) => {
      this.datagetship = deleteshipment;
      this.hidetextgetshipment = true;
      if (deleteshipment.n ==0) {this.textgetshipment = 'Shipment not found'
      }
      else {this.textgetshipment ='Shipment has been deleted'}
      console.log(deleteshipment.n)

                 
      
  });
  }

  
}
