import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  readonly API_URL = 'http://localhost:1234/'
  constructor(private http: HttpClient) { }

    
  connectToNode() {
    return this.http.get(this.API_URL)
  }
  getQuote(data) {
    return this.http.post(`${this.API_URL}getQuote`,data);
  }

  createShipment(data) {
    return this.http.post(`${this.API_URL}createShipment`,data);

  }

  getShip(data) {
    return this.http.post(`${this.API_URL}getShip`,data);

  }

  deleteShip(data) {
    return this.http.post(`${this.API_URL}deleteShip`,data);

  }

  
}
